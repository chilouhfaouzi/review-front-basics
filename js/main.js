document.getElementById("java").addEventListener("click", displayInfo);

function displayInfo() {
  alert(
    "Java is a programming language that is used to create programs. It is used to create web pages"
  );
}

document.getElementById("mygithub").addEventListener("click", getMyGithub);
document.getElementById("search").addEventListener("click", searchInGithub);

function getMyGithub() {
  axios
    .get("https://api.github.com/users/chilouhfaouzi")
    .then(function (response) {
      console.log(response.data);
      // add image
      document.getElementById(
        "mypic"
      ).innerHTML = `<img src="${response.data.avatar_url}">`;

      // add user name
      document.getElementById("username").innerHTML = `${response.data.name}`;
      // add url
      document.getElementById(
        "url"
      ).innerHTML = `<a href="${response.data.html_url}">${response.data.html_url}</a>`;

      // followers
      document.getElementById(
        "followers"
      ).innerHTML = ` number of followers: ${response.data.followers}`;
    });
}

function searchInGithub() {
  var search = document.getElementById("search").value;
  document.getElementById("mypic").style.display = "none";
  axios
    .get(
      `https://api.github.com/search/repositories?q=java%20in:name&per_page=20`
    )
    .then(function (response) {
      console.log(response.data);

      var repos = document.getElementById("repos");

      response.data.items.forEach(function (item) {
        var container = document.createElement("div");
        container.className = "repocontainer";

        //add name
        var tag = document.createElement("div");
        var content = document.createTextNode(`Repository: ${item.full_name}`);
        tag.appendChild(content);
        container.appendChild(tag);

        // add owner login

        var tag = document.createElement("div");
        var content = document.createTextNode(
          `Owner login: ${item.owner.login}`
        );
        tag.appendChild(content);
        container.appendChild(tag);

        // add url
        var tag = document.createElement("div");
        var label = document.createTextNode(`Url: `);
        tag.appendChild(label);
        var content = document.createElement("a");
        content.href = `${item.html_url}`;
        content.textContent = `${item.html_url}`;

        tag.appendChild(content);
        container.appendChild(tag);

        // add forks_count
        var tag = document.createElement("div");
        var content = document.createTextNode(
          `Numbers of fork: ${item.forks_count}`
        );
        tag.appendChild(content);
        container.appendChild(tag);

        repos.appendChild(container);
      });
    });
}
